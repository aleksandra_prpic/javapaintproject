package lista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import geometrijaStek.KvadratStek;
import geometrijaStek.Tacka;
import javax.swing.JScrollBar;
import net.miginfocom.swing.MigLayout;
import javax.swing.JScrollPane;
import java.awt.Color;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class ListaGlavniProzor extends JFrame {

	private JPanel pnlGlavni;
	private ArrayList<KvadratStek> listaKvadrata = new ArrayList<KvadratStek>();
	private DefaultListModel<KvadratStek> dlm = new DefaultListModel<KvadratStek>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ListaGlavniProzor frame = new ListaGlavniProzor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ListaGlavniProzor() {
		setTitle("Lista - Aleksandra Prpić IT58/2015");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 582, 456);
		pnlGlavni = new JPanel();
		pnlGlavni.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlGlavni);
		pnlGlavni.setLayout(new BorderLayout(0, 0));

		JPanel pnlDugmici = new JPanel();
		pnlDugmici.setBackground(new Color(128, 128, 128));
		pnlGlavni.add(pnlDugmici, BorderLayout.NORTH);

		JButton btnDodajKvadrat = new JButton("Dodaj kvadrat u listu");
		btnDodajKvadrat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JTextField xKoordinata = new JTextField();
				JTextField yKoordinata = new JTextField();
				JTextField duzinaStranice = new JTextField();
				JComboBox bojaUnutrasnjosti = new JComboBox();
				JComboBox bojaIvice = new JComboBox();
				bojaUnutrasnjosti.setModel(new DefaultComboBoxModel(
						new String[] { "bela", "crna", "plava", "crvena", "zelena", "žuta", "pink" }));
				bojaIvice.setModel(new DefaultComboBoxModel(
						new String[] { "crna", "bela", "plava", "crvena", "zelena", "žuta", "pink" }));
				final JComponent[] komponente = new JComponent[] { new JLabel("X koordinata"), xKoordinata,
						new JLabel("Y koordinata"), yKoordinata, new JLabel("Dužina stranice"), duzinaStranice,
						new JLabel("Boja unutrašnjosti"), bojaUnutrasnjosti, new JLabel("Boja ivice"), bojaIvice };
				int vrednosti = JOptionPane.showConfirmDialog(null, komponente, "Dodaj kvadrat u listu",
						JOptionPane.PLAIN_MESSAGE);
				if (vrednosti == JOptionPane.OK_OPTION) {
					if (!xKoordinata.getText().isEmpty() && !yKoordinata.getText().isEmpty()
							&& !duzinaStranice.getText().isEmpty()) {
						try {
							if (Integer.parseInt(duzinaStranice.getText()) > 0) {
								KvadratStek privremeni = new KvadratStek(
										new Tacka(Integer.parseInt(xKoordinata.getText()),
												Integer.parseInt(yKoordinata.getText())),
										Integer.parseInt(duzinaStranice.getText()),
										bojaIvice.getSelectedItem().toString());
								listaKvadrata.add(privremeni);
								dlm.addElement(privremeni);

							} else {
								JOptionPane.showMessageDialog(null, "Dužina stranice mora biti veca od 0!", "Greška",
										JOptionPane.ERROR_MESSAGE);
							}
						} catch (NumberFormatException ex) {
							JOptionPane.showMessageDialog(null, "Vrednosti moraju biti celi brojevi!", "Greška",
									JOptionPane.ERROR_MESSAGE);
						}

					} else {
						JOptionPane.showMessageDialog(null, "Sva polja moraju biti popunjena!", "Greška",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		pnlDugmici.setLayout(new MigLayout("insets 4 4 4 4", "[fill,50%][fill,50%]", "[fill,grow]"));
		pnlDugmici.add(btnDodajKvadrat, "cell 0 0,alignx left,aligny top");

		JButton btnSortirajListu = new JButton("Sortiraj listu");
		btnSortirajListu.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				if (!listaKvadrata.isEmpty()) {
					dlm.removeAllElements();

					Collections.sort(listaKvadrata, new Comparator<KvadratStek>() {
						public int compare(KvadratStek k1, KvadratStek k2) {
							return k1.getDuzinaStranice() - k2.getDuzinaStranice();
						}

					});
					for (int i = 0; i < listaKvadrata.size(); i++) {
						dlm.addElement(listaKvadrata.get(i));
					}

				} else {
					JOptionPane.showMessageDialog(null, "Lista kvadrata je prazna!", "Greška!",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		pnlDugmici.add(btnSortirajListu, "cell 1 0,growx,aligny top");

		JPanel pnlSaListom = new JPanel();
		pnlGlavni.add(pnlSaListom, BorderLayout.CENTER);
		pnlSaListom.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();

		JList listaKvadrat = new JList();
		scrollPane.setViewportView(listaKvadrat);
		listaKvadrat.setModel(dlm);
		pnlSaListom.add(scrollPane);
	}

}
