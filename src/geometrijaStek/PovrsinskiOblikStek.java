package geometrijaStek;

import java.awt.Graphics;

public abstract class PovrsinskiOblikStek extends OblikStek {
	private String bojaUnutrasnjosti = "bela";
	
	public abstract void popuni(Graphics g);

	public String getBojaUnutrasnjosti() {
		return bojaUnutrasnjosti;
	}

	public void setBojaUnutrasnjosti(String bojaUnutrasnjosti) {
		this.bojaUnutrasnjosti = bojaUnutrasnjosti;
	}
	
	


}