package stek;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import geometrijaStek.KvadratStek;
import geometrijaStek.Tacka;
import net.miginfocom.swing.MigLayout;

import java.awt.GridBagLayout;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Stack;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.FlowLayout;

public class StekGlavniProzor extends JFrame {

	private JPanel pnlGlavni;
	private Stack<KvadratStek> stekKvadrat = new Stack<KvadratStek>();
	private DefaultListModel<KvadratStek> dlm = new DefaultListModel<KvadratStek>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StekGlavniProzor frame = new StekGlavniProzor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StekGlavniProzor() {
		setTitle("Stek - Aleksandra Prpić IT58/2015");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 717, 488);
		pnlGlavni = new JPanel();
		pnlGlavni.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlGlavni.setLayout(new BorderLayout(0, 0));
		setContentPane(pnlGlavni);

		JPanel pnlDugmici = new JPanel();
		pnlDugmici.setBackground(new Color(128, 128, 128));
		pnlGlavni.add(pnlDugmici, BorderLayout.NORTH);
		GridBagLayout gbl_pnlDugmici = new GridBagLayout();
		gbl_pnlDugmici.columnWidths = new int[] { 400, 167, 0 };
		gbl_pnlDugmici.rowHeights = new int[] { 25, 0 };
		gbl_pnlDugmici.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gbl_pnlDugmici.rowWeights = new double[] { 0.0, Double.MIN_VALUE };

		JButton btnIzuzmiKvadrat = new JButton("Izuzmi kvadrat sa steka");
		btnIzuzmiKvadrat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!stekKvadrat.isEmpty()) {
					KvadratStek privremeni = stekKvadrat.peek();
					final JComponent[] komponente = new JComponent[] {
							new JLabel("X koordinata: " + privremeni.getGoreLevo().getX()),
							new JLabel("Y koordinata: " + privremeni.getGoreLevo().getY()),
							new JLabel("Dužina stranice: " + privremeni.getDuzinaStranice()),
							new JLabel("Boja unutrašnjosti: " + privremeni.getBojaUnutrasnjosti()),
							new JLabel("Boja ivice: " + privremeni.getBoja()) };
					Object[] opcije = new Object[] { "Obrisi", "Odustani" };
					// vraca vrednosti JOptionPane.OK_option odnosno 0 i -1
					int vrednost = JOptionPane.showOptionDialog(null, komponente, "Obriši kvadrat sa steka",
							JOptionPane.PLAIN_MESSAGE, JOptionPane.WARNING_MESSAGE, null, opcije, opcije[0]);
					if (vrednost == JOptionPane.OK_OPTION) {
						dlm.removeElement(privremeni);
						stekKvadrat.pop();
					}
				} else {
					JOptionPane.showMessageDialog(null, "Stek je vec prazan!", "Greška", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		JButton btnDodajKvadrat = new JButton("Dodaj kvadrat u stek");
		btnDodajKvadrat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JTextField xKoordinata = new JTextField();
				JTextField yKoordinata = new JTextField();
				JTextField duzinaStranice = new JTextField();
				JComboBox bojaUnutrasnjosti = new JComboBox();
				JComboBox bojaIvice = new JComboBox();
				bojaUnutrasnjosti.setModel(new DefaultComboBoxModel(
						new String[] { "bela", "crna", "plava", "crvena", "zelena", "žuta", "pink" }));
				bojaIvice.setModel(new DefaultComboBoxModel(
						new String[] { "crna", "bela", "plava", "crvena", "zelena", "žuta", "pink" }));

				final JComponent[] komponente = new JComponent[] { new JLabel("X koordinata"), xKoordinata,
						new JLabel("Y koordinata"), yKoordinata, new JLabel("Duzina stranice"), duzinaStranice,
						new JLabel("Boja unutrašnjosti"), bojaUnutrasnjosti, new JLabel("Boja ivice"), bojaIvice };
				// vraca vrednosti JOptionPane.OK_option odnosno 0 i -1
				int vrednost = JOptionPane.showConfirmDialog(null, komponente, "Dodaj kvadrat na stek",
						JOptionPane.PLAIN_MESSAGE);
				if (vrednost == JOptionPane.OK_OPTION) {
					if (!xKoordinata.getText().isEmpty() && !yKoordinata.getText().isEmpty()
							&& !duzinaStranice.getText().isEmpty()) {
						try {
							if (Integer.parseInt(duzinaStranice.getText()) > 0) {
								KvadratStek privremeni = new KvadratStek(
										new Tacka(Integer.parseInt(xKoordinata.getText()),
												Integer.parseInt(yKoordinata.getText())),
										Integer.parseInt(duzinaStranice.getText()),
										bojaIvice.getSelectedItem().toString());
								stekKvadrat.push(privremeni);
								dlm.addElement(privremeni);
								obrniListu();
							} else {
								JOptionPane.showMessageDialog(null, "Dužina mora biti veća od 0!", "Greška",
										JOptionPane.ERROR_MESSAGE);
							}

						} catch (NumberFormatException ex) {
							JOptionPane.showMessageDialog(null, "Vrednosti moraju biti celi brojevi!", "Greška",
									JOptionPane.ERROR_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(null, "Morate popuniti sva polja!", "Greška",
								JOptionPane.ERROR_MESSAGE);
					}
				}

			}
		});
		pnlDugmici.setLayout(new MigLayout("insets 4 4 4 4", "[fill,50%][fill,50%]", "[fill,grow]"));
		GridBagConstraints gbc_btnDodajKvadrat = new GridBagConstraints();
		gbc_btnDodajKvadrat.insets = new Insets(0, 0, 0, 5);
		gbc_btnDodajKvadrat.gridx = 0;
		gbc_btnDodajKvadrat.gridy = 0;
		pnlDugmici.add(btnDodajKvadrat, "cell 0 0,growx,aligny top");
		GridBagConstraints gbc_btnIzuzmiKvadrat = new GridBagConstraints();
		gbc_btnIzuzmiKvadrat.gridx = 1;
		gbc_btnIzuzmiKvadrat.gridy = 0;
		pnlDugmici.add(btnIzuzmiKvadrat, "cell 1 0,growx,aligny top");

		JPanel pnlStanjeNaSteku = new JPanel();
		pnlStanjeNaSteku.setBackground(new Color(255, 228, 225));
		pnlGlavni.add(pnlStanjeNaSteku, BorderLayout.CENTER);
		pnlStanjeNaSteku.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		pnlStanjeNaSteku.add(scrollPane);

		JList listaKvadrata = new JList();
		scrollPane.setViewportView(listaKvadrata);
		listaKvadrata.setModel(dlm);
	}

	public void obrniListu() {
		dlm.removeAllElements();
		for (int i = stekKvadrat.size() - 1; i >= 0; i--) {
			dlm.addElement(stekKvadrat.get(i));
		}
	}

}
