package crtanje;

import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import geometrija.*;

public class PanelZaCrtanje extends JPanel {

	protected void paintComponent(Graphics arg0) {
		super.paintComponent(arg0);
		ArrayList<Oblik> privremena = CrtanjeGlavniProzor.listaOblika;
		
		for(Oblik obl : privremena){
			obl.crtajSe(arg0);
			if (obl instanceof Krug){
				Krug privremeni = (Krug) obl;
				privremeni.popuni(arg0);
			}
			else if (obl instanceof Kvadrat){
				Kvadrat privremeni = (Kvadrat) obl;
				privremeni.popuni(arg0);
			}else if (obl instanceof Pravougaonik){
				Pravougaonik privremeni = (Pravougaonik) obl;
				privremeni.popuni(arg0);
			}
				
		}
		repaint();
	}
}
