package crtanje;

import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.EventQueue;

import geometrija.*;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.ArrayList;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JToggleButton;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import net.miginfocom.swing.MigLayout;
import java.awt.event.MouseMotionAdapter;

public class CrtanjeGlavniProzor extends JFrame {

	private JPanel pnlGlavni;
	private int brojKlikovaLinije;
	private Linija linijaZaCrtanje;
	private int indeksSelektovanogOblika = -1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CrtanjeGlavniProzor frame = new CrtanjeGlavniProzor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static ArrayList<Oblik> listaOblika = new ArrayList<Oblik>();
	private JComboBox cmbOblici;
	private PanelZaCrtanje pnlCrtanje;
	private JButton btnBojaUnutrasnjosti;
	private JButton btnBojaIvice;

	/**
	 * Create the frame.
	 */
	public CrtanjeGlavniProzor() {

		setTitle("Crtanje - Aleksandra Prpić IT58/2015");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 808, 566);
		pnlGlavni = new JPanel();
		pnlGlavni.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlGlavni.setLayout(new BorderLayout(0, 0));
		setContentPane(pnlGlavni);

		JPanel pnlDugmici = new JPanel();
		pnlDugmici.setBackground(Color.LIGHT_GRAY);
		pnlGlavni.add(pnlDugmici, BorderLayout.NORTH);

		cmbOblici = new JComboBox();
		cmbOblici.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				switch (cmbOblici.getSelectedItem().toString()) {
				case "tacka":
					btnBojaUnutrasnjosti.setEnabled(false);
					break;
				case "linija":
					btnBojaUnutrasnjosti.setEnabled(false);
					break;
				default:
					btnBojaUnutrasnjosti.setEnabled(true);
					break;
				}
			}
		});

		pnlDugmici.setLayout(new MigLayout("insets 4 4 4 4",
				"[fill,20%][fill,16%][fill,16%][fill,16%][fill,16%][fill,16%]", "[fill,grow]"));
		cmbOblici.setModel(
				new DefaultComboBoxModel(new String[] { "tačka", "linija", "krug", "kvadrat", "pravougaonik" }));
		pnlDugmici.add(cmbOblici, "cell 0 0,alignx left,growy");

		JToggleButton tglbtnSelektuj = new JToggleButton("Selektuj");
		tglbtnSelektuj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!tglbtnSelektuj.isSelected()) {
					for(Oblik obl : listaOblika)
						obl.setSelektovan(false);
				}
			}
		});
		pnlDugmici.add(tglbtnSelektuj, "cell 1 0,alignx center,growy");

		JButton btnModifikuj = new JButton("Modifikuj");
		btnModifikuj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!listaOblika.isEmpty()) {
					if (indeksSelektovanogOblika != -1) {
						if (listaOblika.get(indeksSelektovanogOblika) instanceof Tacka) {
							JTextField xKoordinata = new JTextField();
							JTextField yKoordinata = new JTextField();
							JButton btnNovaBoja = new JButton();

							int xKoordinataPrethodna = ((Tacka) listaOblika.get(indeksSelektovanogOblika)).getX();
							xKoordinata.setText(Integer.toString(xKoordinataPrethodna));
							int yKoordinataPrethodna = ((Tacka) listaOblika.get(indeksSelektovanogOblika)).getY();
							yKoordinata.setText(Integer.toString(yKoordinataPrethodna));
							btnNovaBoja.setBackground(listaOblika.get(indeksSelektovanogOblika).getBoja());

							btnNovaBoja.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									btnNovaBoja.setBackground(paletaBoja(btnNovaBoja.getBackground()));
								}
							});
							final JComponent[] komponente = new JComponent[] { new JLabel("Unesite novu X koordinatu"),
									xKoordinata, new JLabel("Unesite novu Y koordinatu"), yKoordinata,
									new JLabel("Odaberite novu boju"), btnNovaBoja };

							int vrednost = JOptionPane.showConfirmDialog(null, komponente, "Modifikovanje tačke",
									JOptionPane.PLAIN_MESSAGE);
							if (vrednost == JOptionPane.OK_OPTION) {
								if (!xKoordinata.getText().isEmpty() && !yKoordinata.getText().isEmpty()) {
									try {
										Integer.parseInt(xKoordinata.getText());
										Integer.parseInt(yKoordinata.getText());
										Tacka privremena = new Tacka(Integer.parseInt(xKoordinata.getText()),
												Integer.parseInt(yKoordinata.getText()));
										privremena.setBoja(btnNovaBoja.getBackground());
										listaOblika.set(indeksSelektovanogOblika, privremena);

									} catch (NumberFormatException ex) {
										JOptionPane.showMessageDialog(null, "Koordinate moraju biti celi brojevi!",
												"Greška!", JOptionPane.ERROR_MESSAGE);
									}

								} else {
									JOptionPane.showMessageDialog(null, "Sva polja moraju biti popunjena!", "Greška!",
											JOptionPane.ERROR_MESSAGE);
								}
							}
						} else if (listaOblika.get(indeksSelektovanogOblika) instanceof Linija) {
							JTextField xKoordinataPocetneT = new JTextField();
							JTextField yKoordinataPocetneT = new JTextField();
							JTextField xKoordinataKrajnjeT = new JTextField();
							JTextField yKoordinataKrajnjeT = new JTextField();
							JButton btnNovaBoja = new JButton();

							int xKoordinataPocetneTPrethodna = ((Linija) listaOblika.get(indeksSelektovanogOblika))
									.gettPocetna().getX();
							xKoordinataPocetneT.setText(Integer.toString(xKoordinataPocetneTPrethodna));
							int yKoordinataPocetneTPrethodna = ((Linija) listaOblika.get(indeksSelektovanogOblika))
									.gettPocetna().getY();
							yKoordinataPocetneT.setText(Integer.toString(yKoordinataPocetneTPrethodna));
							int xKoordinataKrajnjeTPrethodna = ((Linija) listaOblika.get(indeksSelektovanogOblika))
									.gettKrajnja().getX();
							xKoordinataKrajnjeT.setText(Integer.toString(xKoordinataKrajnjeTPrethodna));
							int yKoordinataKrajnjeTPrethodna = ((Linija) listaOblika.get(indeksSelektovanogOblika))
									.gettKrajnja().getY();
							yKoordinataKrajnjeT.setText(Integer.toString(yKoordinataKrajnjeTPrethodna));
							btnNovaBoja.setBackground(listaOblika.get(indeksSelektovanogOblika).getBoja());

							btnNovaBoja.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									btnNovaBoja.setBackground(paletaBoja(btnNovaBoja.getBackground()));
								}
							});
							final JComponent[] komponente = new JComponent[] {
									new JLabel("Unesite novu X koordinatu početne tačke"), xKoordinataPocetneT,
									new JLabel("Unesite novu Y koordinatu početne tačke"), yKoordinataPocetneT,
									new JLabel("Unesite novu X koordinatu krajnje tačke"), xKoordinataKrajnjeT,
									new JLabel("Unesite novu Y koordinatu krajnje tačke"), yKoordinataKrajnjeT,
									new JLabel("Odaberite novu boju"), btnNovaBoja };

							int vrednost = JOptionPane.showConfirmDialog(null, komponente, "Modifikovanje linije",
									JOptionPane.PLAIN_MESSAGE);
							if (vrednost == JOptionPane.OK_OPTION) {
								if (!xKoordinataPocetneT.getText().isEmpty() && !yKoordinataPocetneT.getText().isEmpty()
										&& !xKoordinataKrajnjeT.getText().isEmpty()
										&& !yKoordinataKrajnjeT.getText().isEmpty()) {
									try {
										Integer.parseInt(xKoordinataPocetneT.getText());
										Integer.parseInt(yKoordinataPocetneT.getText());
										Integer.parseInt(xKoordinataKrajnjeT.getText());
										Integer.parseInt(yKoordinataKrajnjeT.getText());
										Linija privremena = new Linija(
												new Tacka(Integer.parseInt(xKoordinataPocetneT.getText()),
														Integer.parseInt(yKoordinataPocetneT.getText())),
												new Tacka(Integer.parseInt(xKoordinataKrajnjeT.getText()),
														Integer.parseInt(yKoordinataKrajnjeT.getText())));
										privremena.setBoja(btnNovaBoja.getBackground());
										listaOblika.set(indeksSelektovanogOblika, privremena);
									} catch (NumberFormatException ex) {
										JOptionPane.showMessageDialog(null, "Koordinate moraju biti celi brojevi!",
												"Greška!", JOptionPane.ERROR_MESSAGE);
									}

								} else {
									JOptionPane.showMessageDialog(null, "Sva polja moraju biti popunjena!", "Greška!",
											JOptionPane.ERROR_MESSAGE);
								}
							}
						} else if (listaOblika.get(indeksSelektovanogOblika) instanceof Pravougaonik) {
							JTextField xKoordinataGoreLevo = new JTextField();
							JTextField yKoordinataGoreLevo = new JTextField();
							JTextField novaVisina = new JTextField();
							JTextField novaSirina = new JTextField();
							JButton btnNovaBojaIvice = new JButton();
							JButton btnNovaBojaUnutrasnjosti = new JButton();

							int xKoordinataGoreLevoPrethodna = ((Pravougaonik) listaOblika
									.get(indeksSelektovanogOblika)).getGoreLevo().getX();
							xKoordinataGoreLevo.setText(Integer.toString(xKoordinataGoreLevoPrethodna));
							int yKoordinataGoreLevoPrethodna = ((Pravougaonik) listaOblika
									.get(indeksSelektovanogOblika)).getGoreLevo().getY();
							yKoordinataGoreLevo.setText(Integer.toString(yKoordinataGoreLevoPrethodna));
							int visinaPrethodna = ((Pravougaonik) listaOblika.get(indeksSelektovanogOblika))
									.getDuzinaStranice();
							novaVisina.setText(Integer.toString(visinaPrethodna));
							int sirinaPrethodna = ((Pravougaonik) listaOblika.get(indeksSelektovanogOblika))
									.getSirina();
							novaSirina.setText(Integer.toString(sirinaPrethodna));
							btnNovaBojaIvice.setBackground(listaOblika.get(indeksSelektovanogOblika).getBoja());
							Color staraBojaUnutrasnjosti = ((Pravougaonik) listaOblika.get(indeksSelektovanogOblika))
									.getBojaUnutrasnjosti();
							btnNovaBojaUnutrasnjosti.setBackground(staraBojaUnutrasnjosti);

							btnNovaBojaIvice.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									btnNovaBojaIvice.setBackground(paletaBoja(btnNovaBojaIvice.getBackground()));
								}
							});
							btnNovaBojaUnutrasnjosti.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									btnNovaBojaUnutrasnjosti
											.setBackground(paletaBoja(btnNovaBojaUnutrasnjosti.getBackground()));
								}
							});
							final JComponent[] komponente = new JComponent[] {
									new JLabel("Unesite novu X koordinatu tačke gore levo"), xKoordinataGoreLevo,
									new JLabel("Unesite novu Y koordinatu tačke gore levo"), yKoordinataGoreLevo,
									new JLabel("Unesite novu visinu"), novaVisina, new JLabel("Unesite novu širinu"),
									novaSirina, new JLabel("Odaberite novu boju ivice"), btnNovaBojaIvice,
									new JLabel("Odaberite novu boju unutrašnjosti"), btnNovaBojaUnutrasnjosti };

							int vrednost = JOptionPane.showConfirmDialog(null, komponente,
									"Modifikovanje pravougaonika", JOptionPane.PLAIN_MESSAGE);
							if (vrednost == JOptionPane.OK_OPTION) {
								if (!xKoordinataGoreLevo.getText().isEmpty() && !yKoordinataGoreLevo.getText().isEmpty()
										&& !novaSirina.getText().isEmpty() && !novaVisina.getText().isEmpty()) {
									try {
										Integer.parseInt(xKoordinataGoreLevo.getText());
										Integer.parseInt(yKoordinataGoreLevo.getText());
										if (Integer.parseInt(novaSirina.getText()) > 0
												&& Integer.parseInt(novaVisina.getText()) > 0) {
											Pravougaonik privremeni = new Pravougaonik(
													new Tacka(Integer.parseInt(xKoordinataGoreLevo.getText()),
															Integer.parseInt(yKoordinataGoreLevo.getText())),
													Integer.parseInt(novaVisina.getText()),
													Integer.parseInt(novaSirina.getText()));
											privremeni.setBoja(btnNovaBojaIvice.getBackground());
											privremeni.setBojaUnutrasnjosti(btnNovaBojaUnutrasnjosti.getBackground());
											listaOblika.set(indeksSelektovanogOblika, privremeni);
										} else {
											JOptionPane.showMessageDialog(null,
													"Visina i širina moraju biti veće od 0!", "Greška!",
													JOptionPane.ERROR_MESSAGE);
										}
									} catch (NumberFormatException ex) {
										JOptionPane.showMessageDialog(null,
												"Koordinate, visina i širina moraju biti celi brojevi!", "Greška!",
												JOptionPane.ERROR_MESSAGE);
									}
								} else {
									JOptionPane.showMessageDialog(null, "Sva polja moraju biti popunjena!", "Greška!",
											JOptionPane.ERROR_MESSAGE);
								}

							}

						}

						else if (listaOblika.get(indeksSelektovanogOblika) instanceof Kvadrat) {
							JTextField xKoordinataGoreLevo = new JTextField();
							JTextField yKoordinataGoreLevo = new JTextField();
							JTextField novaDuzinaStranice = new JTextField();
							JButton btnNovaBojaIvice = new JButton();
							JButton btnNovaBojaUnutrasnjosti = new JButton();

							int xKoordinataGoreLevoPrethodna = ((Kvadrat) listaOblika.get(indeksSelektovanogOblika))
									.getGoreLevo().getX();
							xKoordinataGoreLevo.setText(Integer.toString(xKoordinataGoreLevoPrethodna));
							int yKoordinataGoreLevoPrethodna = ((Kvadrat) listaOblika.get(indeksSelektovanogOblika))
									.getGoreLevo().getY();
							yKoordinataGoreLevo.setText(Integer.toString(yKoordinataGoreLevoPrethodna));
							int duzinaStranicePrethodna = ((Kvadrat) listaOblika.get(indeksSelektovanogOblika))
									.getDuzinaStranice();
							novaDuzinaStranice.setText(Integer.toString(duzinaStranicePrethodna));
							btnNovaBojaIvice.setBackground(listaOblika.get(indeksSelektovanogOblika).getBoja());
							Color staraBojaUnutrasnjosti = ((Kvadrat) listaOblika.get(indeksSelektovanogOblika))
									.getBojaUnutrasnjosti();
							btnNovaBojaUnutrasnjosti.setBackground(staraBojaUnutrasnjosti);

							btnNovaBojaIvice.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									btnNovaBojaIvice.setBackground(paletaBoja(btnNovaBojaIvice.getBackground()));
								}
							});
							btnNovaBojaUnutrasnjosti.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									btnNovaBojaUnutrasnjosti
											.setBackground(paletaBoja(btnNovaBojaUnutrasnjosti.getBackground()));
								}
							});
							final JComponent[] komponente = new JComponent[] {
									new JLabel("Unesite novu X koordinatu tačke gore levo"), xKoordinataGoreLevo,
									new JLabel("Unesite novu Y koordinatu tačke gore levo"), yKoordinataGoreLevo,
									new JLabel("Unesite novu dužinu stranice"), novaDuzinaStranice,
									new JLabel("Odaberite novu boju ivice"), btnNovaBojaIvice,
									new JLabel("Odaberite novu boju unutrašnjosti"), btnNovaBojaUnutrasnjosti };

							int vrednost = JOptionPane.showConfirmDialog(null, komponente, "Modifikovanje kvadrata",
									JOptionPane.PLAIN_MESSAGE);
							if (vrednost == JOptionPane.OK_OPTION) {
								if (!xKoordinataGoreLevo.getText().isEmpty() && !yKoordinataGoreLevo.getText().isEmpty()
										&& !novaDuzinaStranice.getText().isEmpty()) {
									try {
										Integer.parseInt(xKoordinataGoreLevo.getText());
										Integer.parseInt(yKoordinataGoreLevo.getText());
										if (Integer.parseInt(novaDuzinaStranice.getText()) > 0) {
											Kvadrat privremeni = new Kvadrat(
													new Tacka(Integer.parseInt(xKoordinataGoreLevo.getText()),
															Integer.parseInt(yKoordinataGoreLevo.getText())),
													Integer.parseInt(novaDuzinaStranice.getText()));
											privremeni.setBoja(btnNovaBojaIvice.getBackground());
											privremeni.setBojaUnutrasnjosti(btnNovaBojaUnutrasnjosti.getBackground());
											listaOblika.set(indeksSelektovanogOblika, privremeni);
										} else {
											JOptionPane.showMessageDialog(null, "Dužina stranice mora biti veća od 0!",
													"Greška!", JOptionPane.ERROR_MESSAGE);
										}
									} catch (NumberFormatException ex) {
										JOptionPane.showMessageDialog(null,
												"Koordinate i dužina stranice moraju biti celi brojevi!", "Greška!",
												JOptionPane.ERROR_MESSAGE);
									}

								} else {
									JOptionPane.showMessageDialog(null, "Sva polja moraju biti popunjena!", "Greška!",
											JOptionPane.ERROR_MESSAGE);
								}
							}
						}

						else if (listaOblika.get(indeksSelektovanogOblika) instanceof Krug) {
							JTextField xKoordinataCentra = new JTextField();
							JTextField yKoordinataCentra = new JTextField();
							JTextField noviPoluprecnik = new JTextField();
							JButton btnNovaBojaIvice = new JButton();
							JButton btnNovaBojaUnutrasnjosti = new JButton();

							int xKoordinataCentraPrethodna = ((Krug) listaOblika.get(indeksSelektovanogOblika))
									.getCentar().getX();
							xKoordinataCentra.setText(Integer.toString(xKoordinataCentraPrethodna));
							int yKoordinataCentraPrethodna = ((Krug) listaOblika.get(indeksSelektovanogOblika))
									.getCentar().getY();
							yKoordinataCentra.setText(Integer.toString(yKoordinataCentraPrethodna));
							int poluprecnikPrethodni = ((Krug) listaOblika.get(indeksSelektovanogOblika)).getR();
							noviPoluprecnik.setText(Integer.toString(poluprecnikPrethodni));
							btnNovaBojaIvice.setBackground(listaOblika.get(indeksSelektovanogOblika).getBoja());
							Color staraBojaUnutrasnjosti = ((Krug) listaOblika.get(indeksSelektovanogOblika))
									.getBojaUnutrasnjosti();
							btnNovaBojaUnutrasnjosti.setBackground(staraBojaUnutrasnjosti);

							btnNovaBojaIvice.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									btnNovaBojaIvice.setBackground(paletaBoja(btnNovaBojaIvice.getBackground()));
								}
							});
							btnNovaBojaUnutrasnjosti.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent e) {
									btnNovaBojaUnutrasnjosti
											.setBackground(paletaBoja(btnNovaBojaUnutrasnjosti.getBackground()));
								}
							});
							final JComponent[] komponente = new JComponent[] {
									new JLabel("Unesite novu X koordinatu centra"), xKoordinataCentra,
									new JLabel("Unesite novu Y koordinatu centra"), yKoordinataCentra,
									new JLabel("Unesite novi poluprečnik"), noviPoluprecnik,
									new JLabel("Odaberite novu boju ivice"), btnNovaBojaIvice,
									new JLabel("Odaberite novu boju unutrašnjosti"), btnNovaBojaUnutrasnjosti };

							int vrednost = JOptionPane.showConfirmDialog(null, komponente, "Modifikovanje kruga",
									JOptionPane.PLAIN_MESSAGE);
							if (vrednost == JOptionPane.OK_OPTION) {
								if (!xKoordinataCentra.getText().isEmpty() && !yKoordinataCentra.getText().isEmpty()
										&& !noviPoluprecnik.getText().isEmpty()) {
									try {
										Integer.parseInt(xKoordinataCentra.getText());
										Integer.parseInt(yKoordinataCentra.getText());
										if (Integer.parseInt(noviPoluprecnik.getText()) > 0) {
											Krug privremeni = new Krug(
													new Tacka(Integer.parseInt(xKoordinataCentra.getText()),
															Integer.parseInt(yKoordinataCentra.getText())),
													Integer.parseInt(noviPoluprecnik.getText()));
											privremeni.setBojaUnutrasnjosti(btnNovaBojaUnutrasnjosti.getBackground());
											privremeni.setBoja(btnNovaBojaIvice.getBackground());
											listaOblika.set(indeksSelektovanogOblika, privremeni);
										} else {
											JOptionPane.showMessageDialog(null, "Poluprečnik mora biti veći od 0!",
													"Greška!", JOptionPane.ERROR_MESSAGE);
										}
									} catch (NumberFormatException ex) {
										JOptionPane.showMessageDialog(null,
												"Koordinate i poluprečnik moraju biti celi brojevi!", "Greška!",
												JOptionPane.ERROR_MESSAGE);
									}

								} else {
									JOptionPane.showMessageDialog(null, "Sva polja moraju biti popunjena!", "Greška!",
											JOptionPane.ERROR_MESSAGE);
								}
							}
						}
					} else {
						JOptionPane.showMessageDialog(null, "Morate prethodno selektovati oblik!", "Greška!",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Lista oblika je prazna!", "Greška!",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		pnlDugmici.add(btnModifikuj, "cell 2 0,alignx center,growy");

		JButton btnObrisi = new JButton("Obriši");
		btnObrisi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!listaOblika.isEmpty()) {
					if (indeksSelektovanogOblika != -1) {
						Object[] odgovori = new Object[] { "Da", "Ne" };
						int rezultat = JOptionPane.showOptionDialog(null, "Da li ste sigurni da želite da obrišete?",
								"Brisanje", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, odgovori,
								odgovori[0]);
						if (rezultat == JOptionPane.YES_OPTION) {
							listaOblika.remove(indeksSelektovanogOblika);
						}
					} else {
						JOptionPane.showMessageDialog(null, "Morate selektovati oblik!", "Greška!",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Lista oblika je prazna!", "Greška!",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		pnlDugmici.add(btnObrisi, "cell 3 0,alignx center,growy");

		btnBojaUnutrasnjosti = new JButton("Boja unutrašnjosti");
		btnBojaUnutrasnjosti.setEnabled(false);
		btnBojaUnutrasnjosti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnBojaUnutrasnjosti.setBackground(paletaBoja(btnBojaUnutrasnjosti.getBackground()));
			}
		});

		btnBojaIvice = new JButton("Boja ivice");
		btnBojaIvice.setForeground(Color.WHITE);
		btnBojaIvice.setBackground(Color.BLACK);
		btnBojaIvice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnBojaIvice.setBackground(paletaBoja(btnBojaIvice.getBackground()));
			}
		});
		pnlDugmici.add(btnBojaIvice, "cell 4 0,alignx center,growy");
		btnBojaUnutrasnjosti.setBackground(Color.WHITE);
		pnlDugmici.add(btnBojaUnutrasnjosti, "cell 5 0,alignx center,growy");

		pnlCrtanje = new PanelZaCrtanje();
		pnlCrtanje.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				int x = e.getX();
				int y = e.getY();
				if (!tglbtnSelektuj.isSelected()) {
					if (cmbOblici.getSelectedItem() == "tačka") {
						Tacka privremena = new Tacka(x, y, btnBojaIvice.getBackground());
						listaOblika.add(privremena);
					} else if (cmbOblici.getSelectedItem() == "krug") {
						JTextField unosPoluprecnika = new JTextField();
						final JComponent[] komponente = new JComponent[] { new JLabel("Unesite dužinu poluprečnika"),
								unosPoluprecnika };
						int vrednost = JOptionPane.showConfirmDialog(null, komponente, "Unos poluprečnika",
								JOptionPane.PLAIN_MESSAGE);
						if (vrednost == JOptionPane.OK_OPTION) {
							if (!unosPoluprecnika.getText().isEmpty()) {
								try {
									if (Integer.parseInt(unosPoluprecnika.getText()) > 0) {
										Krug privremeni = new Krug(new Tacka(x, y),
												Integer.parseInt(unosPoluprecnika.getText()),
												btnBojaIvice.getBackground());
										privremeni.setBojaUnutrasnjosti(btnBojaUnutrasnjosti.getBackground());
										listaOblika.add(privremeni);
									} else {
										JOptionPane.showMessageDialog(null, "Poluprečnik mora biti veci od 0!",
												"Greška!", JOptionPane.ERROR_MESSAGE);
									}
								} catch (NumberFormatException ex) {
									JOptionPane.showMessageDialog(null, "Poluprečnik mora biti ceo broj!", "Greška!",
											JOptionPane.ERROR_MESSAGE);
								}
							} else {
								JOptionPane.showMessageDialog(null, "Morate uneti poluprečnik!", "Greška!",
										JOptionPane.ERROR_MESSAGE);
							}
						}
					} else if (cmbOblici.getSelectedItem() == "kvadrat") {
						JTextField unosDuzineStranice = new JTextField();
						final JComponent[] komponente = new JComponent[] { new JLabel("Unesite dužinu stranice"),
								unosDuzineStranice };
						int vrednosti = JOptionPane.showConfirmDialog(null, komponente, "Unos dužine stranice",
								JOptionPane.PLAIN_MESSAGE);
						if (vrednosti == JOptionPane.OK_OPTION) {
							if (!unosDuzineStranice.getText().isEmpty()) {
								try {
									if (Integer.parseInt(unosDuzineStranice.getText()) > 0) {
										Kvadrat privremeni = new Kvadrat(new Tacka(x, y),
												Integer.parseInt(unosDuzineStranice.getText()),
												btnBojaIvice.getBackground());
										privremeni.setBojaUnutrasnjosti(btnBojaUnutrasnjosti.getBackground());
										listaOblika.add(privremeni);
									} else {
										JOptionPane.showMessageDialog(null, "Dužina mora biti veća od 0!", "Greška!",
												JOptionPane.ERROR_MESSAGE);
									}
								} catch (NumberFormatException ex) {
									JOptionPane.showMessageDialog(null, "Dužina stranice mora biti ceo broj!",
											"Greška!", JOptionPane.ERROR_MESSAGE);
								}
							} else {
								JOptionPane.showMessageDialog(null, "Morate uneti dužinu stranice!", "Greška!",
										JOptionPane.ERROR_MESSAGE);
							}
						}
					} else if (cmbOblici.getSelectedItem() == "pravougaonik") {
						JTextField unosSirineStranice = new JTextField();
						JTextField unosVisineStranice = new JTextField();
						final JComponent[] komponente = new JComponent[] { new JLabel("Unesite širinu stranice"),
								unosSirineStranice, new JLabel("Unesite visinu stranice"), unosVisineStranice };
						int vrednosti = JOptionPane.showConfirmDialog(null, komponente, "Unos stranica pravougaonika",
								JOptionPane.PLAIN_MESSAGE);
						if (vrednosti == JOptionPane.OK_OPTION) {
							if (!unosSirineStranice.getText().isEmpty() && !unosVisineStranice.getText().isEmpty()) {
								try {
									if (Integer.parseInt(unosSirineStranice.getText()) > 0
											&& Integer.parseInt(unosVisineStranice.getText()) > 0) {
										Pravougaonik privremeni = new Pravougaonik(new Tacka(x, y),
												Integer.parseInt(unosSirineStranice.getText()),
												Integer.parseInt(unosVisineStranice.getText()),
												btnBojaIvice.getBackground());
										privremeni.setBojaUnutrasnjosti(btnBojaUnutrasnjosti.getBackground());
										listaOblika.add(privremeni);
									} else {
										JOptionPane.showMessageDialog(null, "Visina i širina moraju biti veće od 0!",
												"Greška!", JOptionPane.ERROR_MESSAGE);
									}
								} catch (NumberFormatException ex) {
									JOptionPane.showMessageDialog(null, "Visina i sirina moraju biti celi brojevi!",
											"Greška!", JOptionPane.ERROR_MESSAGE);
								}
							} else {
								JOptionPane.showMessageDialog(null, "Morate uneti širinu i visinu!", "Greška",
										JOptionPane.ERROR_MESSAGE);
							}
						}
					} else if (cmbOblici.getSelectedItem() == "linija") {

						if (brojKlikovaLinije == 0) {
							linijaZaCrtanje = new Linija(new Tacka(x, y), new Tacka(0, 0));
							brojKlikovaLinije++;

						} else if (brojKlikovaLinije == 1) {
							linijaZaCrtanje.gettKrajnja().setX(x);
							linijaZaCrtanje.gettKrajnja().setY(y);
							linijaZaCrtanje.setBoja(btnBojaIvice.getBackground());
							listaOblika.add(linijaZaCrtanje);
							brojKlikovaLinije = 0;
						}
					}
				} else {
					prolazakKrozListuOblika(x, y);

				}
			}
		});
		pnlGlavni.add(pnlCrtanje, BorderLayout.CENTER);
	}

	public Color paletaBoja(Color prethodnaBoja) {
		Color privremenaBoja = JColorChooser.showDialog(null, "Odaberite boju", Color.RED);
		if (privremenaBoja != null)
			return privremenaBoja;
		else
			return prethodnaBoja;
	}

	public boolean prolazakKrozListuOblika(int x, int y) {
		if (indeksSelektovanogOblika != -1) {
			listaOblika.get(indeksSelektovanogOblika).setSelektovan(false);
			indeksSelektovanogOblika = -1;
		}
		for (int i = listaOblika.size() - 1; i >= 0; i--) {
			if (listaOblika.get(i).sadrzi(x, y)) {
				listaOblika.get(i).setSelektovan(true);
				indeksSelektovanogOblika = i;
				return true;
			}
		}
		return false;
	}
}
